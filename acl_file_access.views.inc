<?php

/**
 * implements hook_views_data()
 */
function acl_file_access_views_data() {
  // Table: acl
  $data['acl']['table'] = array(
    'base' => array(
      'field' => 'acl_id',
      'title' => t('ACL controls'),
      'help' => t('ACL entries are a list of recorded access control events.'),
      'weight' => 25,
    ),
    'group' => t('ACL'),
  );
  // Table field: acl.acl_id
  $data['acl']['acl_id'] = array(
    'title' => t('aid'),
    'help' => t('Unique acl event ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'aid',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );	
  // Table field: acl.module
  $data['acl']['module'] = array(
    'title' => t('Module Name'),
    'help' => t('The name of the module that created this ACL entry.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  // Table field: acl.name
  $data['acl']['name'] = array(
    'title' => t('ACL Name'),
    'help' => t('A name (or other identifying information) for this ACL entry.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  // Table field: acl.number
  $data['acl']['number'] = array(
    'title' => t('ACL Number'),
    'help' => t('A number for this ACL entry, given by the module that created it.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'field' => 'number',
      'label' => 'Node',
    ),
  );

  // Table : acl_user
  $data['acl_user']['table'] = array(
   'group' => t('ACL'),
    'join' => array(
      'users' => array(
        'field' => 'uid',
        'left_field' => 'uid',
      ),
      'acl' => array(
        'field' => 'acl_id',
        'left_field' => 'acl_id',
      )
    ),
  );
  // Table field: acl_user.acl_id
  $data['acl_user']['acl_id'] = array(
    'title' => t('aid'),
    'help' => t('Unique acl event ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'aid',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );	
  // Table field: acl_user.uid
  $data['acl_user']['uid'] = array(
    'title' => t('User'),
    'help' => t('The ID of the user who is assigned for the access control.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => 'User',
    ),
  );


	return $data;
}

/**
 * implements hook_views_data_alter()
 */
function acl_file_access_views_data_alter(&$data) {
  if (isset($data['download_count'])) {
    // $data['download_count']['table']['join']['node'] = array(
    //   'left_field' => 'nid',
    //   'field' => 'id',
    // );
    $data['download_count']['nid'] = array(
      'title' => t('Node'),
      'help' => t('The node associated with this download'),
      'relationship' => array(
        'handler' => 'views_handler_relationship',
        'base' => 'node',
        'field' => 'id',
        'label' => 'Node',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        'field' => 'id',
      ),
    );
  }
}